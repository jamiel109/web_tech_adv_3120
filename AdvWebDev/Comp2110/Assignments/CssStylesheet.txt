body {
  background-color: lightblue;
}

h1 {
  color: navy;
  margin-left: 20px;
}




     Inline:
	 
	 <body style=background-color:linen;>
	 
	   
	 
	 <h1 style="color:blue;text-align:center;">This is a heading</h1>
     <p style="color:red;">This is a paragraph.</p>
	 
	 <h1 style="background-color:rgb(255, 99, 71);">...</h1>
<h1 style="background-color:#ff6347;">...</h1>
<h1 style="background-color:hsl(9, 100%, 64%);">...</h1>

<h1 style="background-color:rgba(255, 99, 71, 0.5);">...</h1>
<h1 style="background-color:hsla(9, 100%, 64%, 0.5);">...</h1>

div {
  background-color: green;
  opacity: 0.3;
}

div {
  background: rgba(0, 128, 0, 0.3) /* Green background with 30% opacity */
}

    Image:

body {
  background-image: url("paper.gif");
}

body {
  background-image: url("gradient_bg.png");
  background-repeat: repeat-x;
  OR
  background-repeat: no-repeat;
  background-position: right top;
  background-attachment: fixed;   // The Image is Fixed when page scrolls
  background-attachment: scroll;
}

ShortHand Version;
body {
  background: #ffffff url("img_tree.png") no-repeat right top;
}

background-position: right top;
  margin-right: 200px;         Is added to not disturb text

Tip: To repeat an image vertically, set background-repeat: repeat-y;




    External:
	
	<style>
h1 {
  color: orange;
}
</style>
<link rel="stylesheet" type="text/css" href="mystyle.css">
</head>
  
  
  
     Internal:
	 
	 <head>
       <style>
          body { background-color:linen;}
       </style>
       </head>
  
  
