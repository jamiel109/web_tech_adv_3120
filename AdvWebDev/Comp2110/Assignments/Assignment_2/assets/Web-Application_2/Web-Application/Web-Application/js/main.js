/*
 *
 * Module: <name>
 * < short description here e.g. "This module implements main entry point...">
 *
 * Student Name:
 * Student Number:
 *
 */



function redraw(targetid, rimage) { 

    // console.log(window.location.hash);

    // let random = "<h2>API Test</h2><ul>";
    // random += "<li><a href='/#'>Three Posts</a></li>";
    // random += "</ul>";

    // let content = "<ul><li><a href='/#'>Recent Posts</a></li>";
    // content += "<li><a href='/#'>Popular Posts</a></li>";
    // content += "<li><a href='/posts/1'>A Single Post</a></li>"; 
    // content += "</ul>";

    // update the page
    // document.getElementById("target_random").innerHTML = random;
    // document.getElementById("target_recpop").innerHTML = content;

    let target = document.getElementById(targetid);

    let template = Handlebars.compile(
                                document.getElementById("random_image_table").textContent
                                );

    let list = template({'rimage': rimage});
    console.log(list);

    target.innerHTML = list;
}

function load_images() {

    fetch('/js/sample.json')
    .then(
        function(response) {
            return response.json();
        }
    )
    .then(
        function(data){
            console.log(data);

            redraw("content", data);
        }
    )

}

window.onload = function() {
    
    load_images();
    
};


