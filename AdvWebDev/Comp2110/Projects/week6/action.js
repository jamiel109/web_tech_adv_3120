export {buttonIsSort, buttonIsRevert};
import {listPeopleView} from './views.js';

/*
    Action Functions......

*/

function buttonIsSort(buttonid, people) {

    let button = document.getElementById(buttonid);
    button.innerText = "Sort";

    button.onclick = function () {
        let sorted_people = people.slice();
        sorted_people.sort(function (a, b) {
            return a.name > b.name;
        });
        listPeopleView("content", sorted_people);
        buttonIsRevert(buttonid, people);
    }
}


function buttonIsRevert(buttonid, people) {

    let button = document.getElementById(buttonid);
    button.innerText = "Revert";

        button.onclick = function () {
            listPeopleView("content", people);
            buttonIsSort(buttonid, people);
    }

}