export { listPeopleView };


/*
   Sort button sorts people list  
*/
function listPeopleView(targetid, people) {

    let target = document.getElementById(targetid);

    let template = Handlebars.compile(
               document.getElementById("people-list-template").textContent
           );

    let list = template({ 'people': people });
    console.log(list);
    
    target.innerHTML = list;
}