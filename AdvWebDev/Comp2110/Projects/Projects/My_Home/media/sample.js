/*
   I need to create these objects in my  page and put an external reference to the sample js page
*/

let button1 = document.getElementById('button1')
let button2 = document.getElementById('button2')

function change_first_text(text) {
    let first = document.getElementById('first');
    first.innerHTML = text;
}

button1.onclick = function() {
    change_first_text("Hello_World_again");
}

button2.onclick = function() {
    change_first_text("Goodbye_World_for_Now!!!")
}