import {useState} from 'react'

const Unit = (props) => {

    const [title, setTitle] = useState(props.title)

    return (
        <div className="unit">
            <h2>{props.code} {title}</h2>
            <button onClick={() => setTitle(title.toUpperCase())}>Up</button>
            <button onClick={() => setTitle(title.toLowerCase())}>Down</button>
        </div>
    )
}

export default Unit;